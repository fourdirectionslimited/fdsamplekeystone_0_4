# ##	Sample project commit reading guidline
*	How to change node_module version in package.json.
*	Set environment configuration in config.
*	How to set PM2 process json for different server environment.
*	How to build package.js for front end javascript
*	How to compress images.
*	How to do admin UI customization
*	How to change admin path

---
# ##To run Project
#### Please do not use project_name with spacing (you may replace spaces with underscores) and lower case is suggested
```bash
$ cd /path/to/repo/destination/
$ git clone https://bitbucket.org/fourdirectionslimited/fdsamplekeystone_0_4.git
$ cd fdsamplekeystone_0_4
$ npm run initiate
```

# ##To install plugins
* Add your plugins repo link to plugin.json.

* Run install plugin command
```bash
$ npm run install-plugins
```
