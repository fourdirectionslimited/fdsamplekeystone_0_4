import React from 'react';

import Profile from 'member/components/Profile';

const MemberPage = () => <Profile />;

export default MemberPage;
