import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Link } from 'react-router-dom';

import MemberPage from './Member';
import NewsPage from './News';
import Header from 'app/components/Header';
import Footer from 'app/components/Footer';

import { initApp } from 'app/reducers/state';

class HomeView extends Component {
    componentDidMount() {
        const { initApp } = this.props;
        initApp();
    }

    render() {
        const { initialized, error } = this.props;

        if (error) {
            return <div>{error.message}</div>;
        }

        if (!initialized) {
            return <div>loading</div>;
        }

        return (
            <div>
                <Header />
                <ul>
                    <li>
                        <Link to="/member">Member Page</Link>
                    </li>
                    <li>
                        <Link to="/news">News Page</Link>
                    </li>
                </ul>
                <Route path="/member" component={MemberPage} />
                <Route path="/news" component={NewsPage} />
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    initialized: state.app.state.initialized,
    error: state.app.state.error
});

const mapDispatchToProps = {
    initApp
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);
