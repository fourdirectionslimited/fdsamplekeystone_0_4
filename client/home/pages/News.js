import React from 'react';

import NewsList from 'news/containers/NewsList';

const NewsPage = () => <NewsList />;

export default NewsPage;
