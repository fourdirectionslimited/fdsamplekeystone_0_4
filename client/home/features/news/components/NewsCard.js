import React from 'react';
import styled from 'styled-components';

const Card = styled.div`
    border: 1px black solid;
    background-color: brown;
`;

const Header = styled.h4`
    font-size: 30px;
`;

const NewsCard = ({ news }) => (
    <Card>
        <Header>{news.name}</Header>
        <p>{news.content}</p>
        <a>{news.url}</a>
    </Card>
);

export default NewsCard;
