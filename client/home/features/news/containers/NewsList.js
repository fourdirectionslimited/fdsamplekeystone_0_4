import React, { Component } from 'react';
import { connect } from 'react-redux';

import NewsCard from 'news/components/NewsCard';

import { fetchNewsList } from 'news/reducers/list';

class NewsList extends Component {
    componentDidMount() {
        const { fetchNewsList } = this.props;
        fetchNewsList();
    }

    render() {
        const { isFetching, fetchingError, newsList } = this.props;

        if (isFetching) {
            return <div>loading</div>;
        }

        if (fetchingError) {
            return <div>{fetchingError.message}</div>;
        }

        if (newsList.length === 0) {
            return <div>No News</div>;
        }

        return (
            <div>
                {newsList.map(news => <NewsCard key={news._id} news={news} />)}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    newsList: state.news.list.data,
    isFetching: state.news.list.isFetching,
    fetchingError: state.news.list.fetchingError
});

const mapDispatchToProps = {
    fetchNewsList
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
