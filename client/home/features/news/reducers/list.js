import api from './../../app/api';

export const FETCHING_NEWS_LIST = 'news/list/FETCHING_NEWS_LIST';
export const FETCHING_NEWS_LIST_SUCCESS =
    'news/list/FETCHING_NEWS_LIST_SUCCESS';
export const FETCHING_NEWS_LIST_FAILURE =
    'news/list/FETCHING_NEWS_LIST_FAILURE';

export const fetchingNewsList = () => ({
    type: FETCHING_NEWS_LIST
});
export const fetchingNewsListSuccess = data => ({
    type: FETCHING_NEWS_LIST_SUCCESS,
    data
});
export const fetchingNewsListFailure = error => ({
    type: FETCHING_NEWS_LIST_FAILURE,
    error
});

export const fetchNewsList = () => async dispatch => {
    dispatch(fetchingNewsList());

    try {
        const response = await fetch(`${api.urls.news_url}`);

        let body = {};
        try {
            body = await response.json();
        } catch (error) {
            body = {};
        }

        if (response.ok) {
            dispatch(fetchingNewsListSuccess(body));
            return;
        }
        throw new Error(body.message);
    } catch (error) {
        dispatch(fetchingNewsListFailure(error));
        throw error;
    }
};

const initialState = {
    data: [],
    isFetching: false,
    fetchingSuccess: false,
    fetchingError: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCHING_NEWS_LIST: {
            return {
                ...state,
                isFetching: true,
                fetchingSuccess: false,
                fetchingError: null
            };
        }
        case FETCHING_NEWS_LIST_SUCCESS: {
            return {
                ...state,
                data: action.data,
                isFetching: false,
                fetchingSuccess: true,
                fetchingError: null
            };
        }
        case FETCHING_NEWS_LIST_FAILURE: {
            return {
                ...state,
                isFetching: false,
                fetchingSuccess: false,
                fetchingError: action.error
            };
        }
        default: {
            return state;
        }
    }
};
