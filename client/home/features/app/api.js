import { API_ENDPOINT_ADDRESS } from './../../constants';

const api = {};

export const getApiEndpoints = function getApiEndpoints() {
    return fetch(API_ENDPOINT_ADDRESS)
        .then(response => response.json())
        .then(responseJson => {
            api.urls = responseJson;
        });
};

export default api;
