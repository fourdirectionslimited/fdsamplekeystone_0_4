import { getApiEndpoints } from './../api';

export const INIT_APP_SUCCESS = 'app/state/INIT_APP_SUCCESS';
export const INIT_APP_FAILURE = 'app/state/INIT_APP_FAILURE';

export const initAppSuccess = () => ({ type: INIT_APP_SUCCESS });
export const initAppFailure = error => ({ type: INIT_APP_FAILURE, error });

export const initApp = () => async dispatch => {
    try {
        await getApiEndpoints();
        dispatch(initAppSuccess());
    } catch (error) {
        dispatch(initAppFailure(error));
    }
};

const initialState = {
    initialized: false,
    error: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case INIT_APP_SUCCESS: {
            return {
                ...state,
                initialized: true
            };
        }
        case INIT_APP_FAILURE: {
            return {
                ...state,
                error: action.error
            };
        }
        default: {
            return state;
        }
    }
};
