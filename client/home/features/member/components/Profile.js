import React from 'react';

import ProfilePicture from 'member/components/ProfilePicture';
import Biography from 'member/components/Biography';

const Profile = () => (
    <div>
        <ProfilePicture />
        <Biography />
    </div>
);

export default Profile;
