import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import appReducers from 'app/reducers';
import newsReducers from 'news/reducers';

const reducers = combineReducers({
    app: appReducers,
    news: newsReducers
});

// Create the store
const store = createStore(
    reducers,
    compose(
        applyMiddleware(
            // Support thunked actions
            thunk
        ),
        // Support the Chrome DevTools extension
        window.devToolsExtension && process.env.NODE_ENV !== 'production'
            ? window.devToolsExtension()
            : f => f
    )
);

store.subscribe(() => {});

export default store;
