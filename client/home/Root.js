import React from 'react';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import App from './pages';
import store from './store';

const BlockAvoider = withRouter(App);

const Root = () => (
    <Provider store={store}>
        <Router>
            <BlockAvoider />
        </Router>
    </Provider>
);

export default Root;
