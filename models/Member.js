const keystone = require('keystone');

const EnhancedList = require(`${global.__base}/models/lib/EnhancedList`);

const memberEnhancedList = new EnhancedList('Member', {
    track: true,
    defaultColumns: 'name, email'
});

const Types = keystone.Field.Types;
memberEnhancedList.add({
    name: { type: Types.Text, required: true, initial: true },
    email: { type: Types.Text },
    password: { type: Types.Password },
    phone: { type: Types.Text },
    address: { type: Types.Text }
});

memberEnhancedList.defaultSelectOption = '-password';
memberEnhancedList.register();

module.exports = memberEnhancedList;
