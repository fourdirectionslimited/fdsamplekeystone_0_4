const keystone = require('keystone');

const localize = require(`${global.__base}/models/lib/localize`);
const EnhancedList = require(`${global.__base}/models/lib/EnhancedList`);

const newsEnhancedList = new EnhancedList('News', {
    track: true,
    defaultColumns: 'name, url'
});

const Types = keystone.Field.Types;
newsEnhancedList.add(
    localize({
        name: { type: Types.Text, required: true, initial: true },
        url: { type: Types.Url },
        content: { type: Types.Text }
    })
);

newsEnhancedList.register();

module.exports = newsEnhancedList;
