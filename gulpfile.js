var gulp = require('gulp');
var inquirer = require('inquirer');
var template = require('gulp-template');
var chalk = require('chalk');
// var watch = require('gulp-watch');
// var shell = require('gulp-shell');
var path = require('path');

// var sass = require('gulp-sass');

// var argv = require('optimist').argv;
var paths = {
	src: ['./models/**/*.js', './routes/**/*.js', 'keystone.js', 'package.json'],

	style: {
		all: './public/styles/**/*.scss',
		output: './public/styles/',
	},

};
function log(msg){
	console.log(chalk.bold.cyan(msg || ''))
}
function error(msg){
	console.log(chalk.bold.red(msg || ''))
}
function success(msg){
	console.log(chalk.bold.green(msg || ''))
}
//
// gulp.task('watch:sass', function () {
// 	gulp.watch(paths.style.all, ['sass']);
// });
//
// gulp.task('sass', function () {
// 	gulp.src(paths.style.all)
// 		.pipe(sass().on('error', sass.logError))
// 		.pipe(gulp.dest(paths.style.output));
// });
function buildConfig(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/Config.js')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/config'))
		.on('end', resolve)
	})
}
function buildKeystoneJs(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/keystone.js')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/'))
		.on('end', resolve)
	})
}
function buildPM2(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/PM2/*.json')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/PM2Processes'))
		.on('end', resolve)
	})
}

function buildDefaultUpdate(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/Updates/*.js')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/updates'))
		.on('end', resolve)
	})
}
function buildPackageJson(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/package.json')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/'))
		.on('end', resolve)
	})
}
function buildReadme(res){
	return new Promise(function(resolve, reject){
		gulp.src('./GulpTemplate/README.md')
		.pipe(template(res))
		.pipe(gulp.dest('./tmp/'))
		.on('end', resolve)
	})
}
gulp.task('init', function(done){
	inquirer.prompt([
		{type: 'input', name: 'Location', message: 'Destination of Application', default: path.resolve('../')},
		{type: 'input', name: 'Application', message: 'Name the Application', default: 'FDSampleProject'},
		{type: 'confirm', name: 'lowerCase', message: 'Change the application name to lowerCase', default: true},
		{type: 'input', name: 'dbName', message: 'Name the database', default: 'FDSampleProjectDb'},
	    {type: 'input', name: 'username', message: 'Enter default username for CMS', default: 'admin@4d.com.hk'},
	    {type: 'password', name: 'password', message: 'Enter default password for CMS', default: 'Password'},

	]).then((res)=>{
		if(res.lowerCase){
			res.Application = res.Application.toLowerCase();
		}
		var async = require('async');
		var shell = require('shelljs');
		var DirectoryPath = res.Location+'/'+res.Application;

		Promise.all([
			buildConfig(res),
			buildPM2(res),
			buildKeystoneJs(res),
			buildDefaultUpdate(res),
			buildPackageJson(res),
			buildReadme(res),
		]).then(()=>{
			var async = require('async');
			var shell = require('shelljs');
			shell.mkdir('-p', DirectoryPath);
			var FilesList = require('./RequiredFiles').default;
			FilesList = FilesList.concat([
				{source:'tmp/config', destination:'.'},
				{source:'tmp/PM2Processes', destination:'.'},
				{source:'tmp/updates', destination:'.'},
				{source:'tmp/package.json', destination:'.'},
				{source:'tmp/keystone.js', destination:'.'},
				{source:'tmp/README.md', destination:'.'}
			]);
			log("Copying Required files to destination");
			async.eachSeries(FilesList, function(file, callback){
				var source, destination;
				if(typeof file !== 'string'){
					source=file.source;
					destination = `${DirectoryPath}/${file.destination}`;
				}else{
					source=file;
					destination = `${DirectoryPath}/`;
				}
				log(`Copying from ${source} to ${destination}`)
				var command;
				if(path.extname(destination)){
					command = `cp -r ${source} ${destination}`;
				}else{
					command = `test -d ${destination} || mkdir -p ${destination} && cp -r ${source} ${destination}`;
				}
				shell.exec(command, function(code, stdout, stderr) {
					if(stderr){
						error(stderr);
						callback(new Error(stderr))
					}else{
						log('Copied')
						console.log("");//line break only;
						callback()
					}
				})
			}, function(){
				success("Required Files copied\r\n");
				success('Going to install node_modules');
				shell.exec(`rm -rf ./tmp && cd ${DirectoryPath} && npm install`, function(code, stdout, stderr) {
					success("Your Project has been initialized in "+DirectoryPath);
					shell.exec(`cd ${DirectoryPath} && git init`, function(code, stdout, stderr) {
						success('git initialized');
						error(stderr);
						done();
					})

				});
			})
		});

	})

})
// gulp.task('runKeystone', shell.task('node keystone.js'));
// gulp.task('watch', [
//
// 	'watch:sass',
//
// ]);
//
// gulp.task('default', ['watch', 'runKeystone']);
