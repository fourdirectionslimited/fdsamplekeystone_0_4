// store the base path
global.__base = __dirname;

var Config = require(global.__base + '/config/Config');
var conf = new Config();

var KeystoneMode = require(global.__base + '/enum/KeystoneMode');

// Require keystone
var keystone = require('keystone');

var fs = require('fs');
var Utility = require(global.__base + '/helper/Utility');

var dbUrl = 'mongodb://' + conf.dbHost + '/' + conf.dbName;
if (conf.dbAuthEnable) {
	dbUrl = 'mongodb://' + conf.dbUser + ':' + conf.dbPassword + '@' + conf.dbHost + '/' + conf.dbName;
}

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.
var isCMS = !(conf.keystoneMode === KeystoneMode.api.key);
keystone.init({
	'name': '<%= Application %>',
	'brand': '<%= Application %>',
	'mongo': dbUrl,
	'port': conf.port,

	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'pug',

	'auto update': isCMS,
	'session store': 'mongo',
	'session store options': {
		ttl: 60 * 60,
	},
	'session': isCMS,
	'auth': isCMS,
	'user model': 'User',
	'headless': !isCMS,
	'admin path': 'admin',
	'cookie secret': 'P8@D/30,Svd1TL1a6]17%;t6QM13>4:OEbu3o#K8bTu>;SRT-!mFeK1plGjan5a',
	enhancedList: []
});

keystone.enhancedList = function enhancedList(name) {
    if (name) {
        return keystone
            .get('enhancedList')
            .find(enhancedList => enhancedList.name === name.toLowerCase());
    }
    return keystone.get('enhancedList');
};

// Load your project's Models
keystone.import('models');

// Load your project's plugins' Models
var pluginPath = global.__base + '/plugins';
try {
	var plugins = Utility.getDirectories(pluginPath);
	plugins.forEach(function(plugin){
		try {
			keystone.import('plugins/' + plugin + '/models');
		}
		catch (err){
			console.log('Plugins models not exist!');
		}
	})
}
catch (err){
	console.log('Plugins folder not exist!');
}

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

// Load your project's Routes
keystone.set('routes', require('./routes'));
keystone.set('pre:dynamic', require('./routes/bindBodyParser')); // this is required for audit-trail plugin; should be removed/implemented in other way.
keystone.set('pre:static', require('./routes/admin'));

// Configure the navigation bar in Keystone's Admin UI
keystone.set('nav', {
	users: 'users',
});

// Start Keystone to connect to your database and initialise the web server

keystone.start();
