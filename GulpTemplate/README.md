# # <%= Application %>
# ##	Sample project commit reading guidline
*	How to change node_module version in package.json.
*	Set environment configuration in config.
*	How to set PM2 process json for different server environment.
*	How to build package.js for front end javascript
*	How to compress images.
*	How to do admin UI customization
*	How to change admin path

---
# ##To run Project
#### Please do not use project_name with spacing (you may replace spaces with underscores) and lower case is suggested
```bash
$ node keystone.js / npm start
```

# ##To install plugins
#### Add your plugins to plugin.json
```bash
$ npm run install-plugins
```
