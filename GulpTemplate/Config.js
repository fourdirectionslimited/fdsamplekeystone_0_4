const _ = require('lodash');

const AppEnv = require(global.__base + '/enum/AppEnv');
const KeystoneMode = require(global.__base + '/enum/KeystoneMode');
const ClientPlatform = require(global.__base + '/enum/ClientPlatform');

// Common setting for all environment
const commonSetting = {
    port: 3000
};

const localhostSetting = {
    dbHost: 'localhost',
    dbName: '<%= dbName %>',
	dbLogName: '<%= dbName %>log',
    dbAuthEnable: false,
    dbUser: '[dbusername]',
    dbPassword: '[dbpassword]',
    dbPort: 27017,
    appDomain: 'localhost:3000',

    newRelicName: '[projectname]localhost',
    enableHttp: true
};

const developmentSetting = {
    dbHost: 'localhost',
    dbName: '<%= dbName %>',
	dbLogName: '<%= dbName %>log',
    dbAuthEnable: false,
    dbUser: '[dbusername]',
    dbPassword: '[dbpassword]',
    dbPort: 27017,

    newRelicName: '[projectname]dev',
    enableHttp: true
};

const betaSetting = {
    dbHost: 'localhost',
    dbName: '<%= dbName %>',
	dbLogName: '<%= dbName %>log',
    dbAuthEnable: false,
    dbUser: '[dbusername]',
    dbPassword: '[dbpassword]',
    dbPort: 27017,

    newRelicName: '[projectname]beta',
    enableHttp: true
};

const productionSetting = {
    dbHost: '',
    dbName: '<%= dbName %>',
	dbLogName: '<%= dbName %>log',
    dbAuthEnable: true,
    dbUser: '[dbusername]',
    dbPassword: '[dbpassword]',
    dbPort: 27017,

    newRelicName: '[projectname]prod',
    enableHttp: false
};

function Config() {
    this.appEnv = process.env.APP_ENV || AppEnv.localhost.key;
    this.clientPlatform = process.env.CLIENT_PLATFORM || ClientPlatform.web.key;
    this.keystoneMode = process.env.KEYSTONE_MODE || KeystoneMode.cms.key;
    this.stressTestMode = process.env.STRESSTESTMODE || null;

    _.extend(this, commonSetting);

    switch (this.appEnv) {
        case AppEnv.localhost.key:
            _.extend(this, localhostSetting);
            break;
        case AppEnv.development.key:
            _.extend(this, developmentSetting);
            break;
        case AppEnv.beta.key:
            _.extend(this, betaSetting);
            break;
        case AppEnv.production.key:
            _.extend(this, productionSetting);
            break;
        default:
            _.extend(this, localhostSetting);
    }

    if (this.keystoneMode === KeystoneMode.cms.key) {
        this.newRelicName = `${this.newRelicName}_cms`;
    }
}

module.exports = Config;
