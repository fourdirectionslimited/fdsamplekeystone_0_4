var directories=[
    "client",
    "enum",
    "helper",
    "imagetools",
    "manager",
    "models",
    "public",
    "routes",
    "scripts",
    "templates"
];
var hiddenFiles=[
    ".babelrc",
    ".editorconfig",
    ".eslintignore",
    ".eslintrc",
    ".gitignore",
]
var JavascriptFiles=[
    "plugin.json",
    "Procfile",
    "webpack.config.js",
    "webpack.admin.config.js"
]

exports.default = [].concat(directories, hiddenFiles, JavascriptFiles)
