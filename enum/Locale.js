const Enum = require('enum');

const Utility = require(`${global.__base}/helper/Utility`);

const Locale = new Enum({
    'zh-HK': 0,
    'en-US': 1
});

Locale.options = Utility.enumToKeystonOptionStr(Locale);

module.exports = Locale;
